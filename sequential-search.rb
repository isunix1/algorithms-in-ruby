#!/usr/bin/env ruby
# encoding: utf-8

def sequential_search(array, length, key)
  for i in 1..length
    if array[i] == key
      return i
    end
  end
end

array = [1, 3, 4, 5, 5]
key = 5
length = 5

puts sequential_search(array, length, key)
#will give out 3

