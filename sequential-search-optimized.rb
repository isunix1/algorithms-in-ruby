#!/usr/bin/env ruby
# encoding: utf-8
# have no idea why this program does not work

def sequential_search_optimized(array, length, key)
  array[0] = key
  i = length
  while array[i] != key
    i--
  end
  return i
end

array = [1, 2, 3, 5, 5]
length = array.size
key = 5
p sequential_search_optimized(array, length, key)
