#!/usr/bin/env ruby
# encoding: utf-8
# to realize the function of binary search.
#
def binary_search(array, length, key)
  low = 1
  high = length
  while low <= high
    mid = (low + high) / 2
    if key < array[mid]
      high = mid - 1
    elsif key > array[mid]
      low = mid + 1
    else
      return mid
    end
  end
end

array = [0, 1, 16, 24, 35, 47, 59, 62, 73, 88, 99]
length = array.size
key = 62

puts binary_search(array, length, key)
